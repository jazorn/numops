import setuptools

setuptools.setup(
	name='NumericalOptimization',
	version='0.2.0',
	author='Jacob Zorn',
	author_email='jzz164@psu.edu',
	description='A package for optimization',
	packages=setuptools.find_packages())
	