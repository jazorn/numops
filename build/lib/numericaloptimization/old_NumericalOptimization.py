"""
Numerical Optimization Library
Written by Jacob Zorn
Date Created: 11/5/2019
Date Updated: 11/5/2019
Version: 0.1.0

I need to run a study on the performance of each of the mutation strategies I have here so I can make sure, we use the best one.
"""

#Import Outside Libraries
import numpy as np
import sys
import numdifftools as nd
import matplotlib.pyplot as plt

#------------------------

#Codes

class Routines:
	
	def Check_Args(Prob, Args, Bounds):
	
		if type(Args) is list:
			args_count = len(Args) + len(Bounds)
		else:
			args_count = len(Bounds)
			
		prob_parameters = Prob.__code__.co_argcount
		
		if prob_parameters != args_count:
			sys.exit('The number of parameters is not consistent. Quitting Optimization, please address in your problem statement.')
		
	def Repopulate_Vector(Bounds):
		
		min_b, max_b 	= np.asarray(Bounds).T
		diff			= np.fabs(min_b - max_b)
		return_vec		= min_b + np.random.rand(1,len(Bounds)) * diff
		return return_vec[0]
		
	def Calculate_Fitness(Vector, Prob, Args=''):
		
		if Args == '':
			#print('Printing Vector {}'.format(Vector))
			return Prob(*Vector)
		else:
			return Prob(*np.concatenate(Vector,Args))
		
	def Population_Setup(Popsize, Bounds):
	
		Population 	= np.random.rand(Popsize,len(Bounds))
		boundLen 	= len(Bounds)
		
		min_b, max_b	= np.asarray(Bounds).T
		diff			= np.fabs(min_b - max_b)
		pop_denorm		= min_b + Population * diff
		
		return pop_denorm
		
	def Inital_Fitness(Population, Args, Prob):
		
		if Args != '':
			fitness	= np.asarray([Prob(*np.concatenate((ind,Args))) for ind in Population])
		else:
			fitness = np.asarray([Prob(*ind) for ind in Population])
		
		return fitness
		
	def Elitest_Selection(Parent, Parent_Fitness, Child, Child_Fitness):
		
		if Child_Fitness < Parent_Fitness:
			return Child
		else:
			return Parent
			
	def Sim_Anneal_Temperature_Change(Iteration, Max_Iterations, T0):
		
		Tnew = T0 * np.exp(-Iteration / Max_Iterations)
		return Tnew
	
	def Multimodal_Strategy_Selection(Population, Mutation, Strategy, NN, Best, avail_idxs):
		
		best = Best
		current = NN
		pop_denorm = Population
		mutation = Mutation
		strategy = Strategy
		
		if strategy is 'rand1bin':
			a, b, c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = NN + mutation * (b - c)
		elif strategy is 'rand2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = NN + mutation * (b + d - c - e)
		elif strategy is 'best1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = best + mutation * (b - c)
		elif strategy is 'best2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = best + mutation * (b + d - c - e)
		elif strategy is 'currentBest1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = a + mutation * (best - current + b - c)
		elif strategy is 'currentBest2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = current + mutation * (best - current + b + d - c - e)
		elif strategy is 'bestCurrent1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = best + mutation * (current - b)
		elif strategy is 'bestCurrent2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = best + mutation * (current + d - c - e)
		elif strategy is 'currentRand1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = current + mutation * (b - c)
		elif strategy is 'currentRand2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = current + mutation * (b - c + d - e)
		elif strategy is 'randCurrent1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = a + mutation * (current - c)
		elif strategy is 'randCurrent2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = a + mutation * (current - c + d - e)
			
		return mutant
	
	def Clip_Mutant(Mutant, Bounds):
		
		for bound, i in zip(Bounds,range(len(Mutant))):
			Mutant[i] = np.clip(Mutant[i],bound[0],bound[1])
			
		return Mutant
	
	def Strategy_Selection(Population, Mutation, Strategy, Current, Best, avail_idxs):
	
		best = Best
		current = Current
		pop_denorm = Population
		mutation = Mutation
		strategy = Strategy
		
		if strategy is 'rand1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = a + mutation * (b - c)
		elif strategy is 'rand2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = a + mutation * (b + d - c - e)
		elif strategy is 'best1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = best + mutation * (b - c)
		elif strategy is 'best2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = best + mutation * (b + d - c - e)
		elif strategy is 'currentBest1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = current + mutation * (best - current + b - c)
		elif strategy is 'currentBest2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = current + mutation * (best - current + b + d - c - e)
		elif strategy is 'bestCurrent1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = best + mutation * (current - b)
		elif strategy is 'bestCurrent2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = best + mutation * (current + d - c - e)
		elif strategy is 'currentRand1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = current + mutation * (b - c)
		elif strategy is 'currentRand2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = current + mutation * (b - c + d - e)
		elif strategy is 'randCurrent1bin':
			a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
			mutant = a + mutation * (current - c)
		elif strategy is 'randCurrent2bin':
			a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
			mutant = a + mutation * (current - c + d - e)
		
		return mutant
			
	def Crossover_Selection(boundsLen, CrossPollinator, Current, Mutant):
	
		cross_points = np.random.rand(boundsLen) < CrossPollinator
		cross_points[np.random.randint(0,boundsLen)] = True
		return np.where(cross_points, Mutant, Current)
		
	def Hill_Valley_Clustering(Population, Fitness, Prob, Args=''):
		
		sort_pop = np.concatenate((Population, Fitness.reshape(len(Fitness),1)),axis=1)
		sort_pop = sort_pop[sort_pop[:,Population.shape[1]].argsort()]
		
		#Population is sorted according to the paper
		
		cluster_dict = {}
		cluster_dict[0] = [0]
		
		#For each individual in the population
		for i in range(1, len(Population)-1):
			HVFlag 			= False
			break_flag		= False
			
			#Find the distance between a given individual and the rest of the population that has been surveyed thus far.
			dist_list 		= []
			for j in range(i):
				dist_list.append([np.linalg.norm(sort_pop[i,:Population.shape[1]] - sort_pop[j,:Population.shape[1]]),j])
			dist_list 		= np.asarray(dist_list)
			dist_list 		= dist_list[dist_list[:,0].argsort()]
			
			#Now we wanna go through the distance array and if they belong to a cluster
			for j in range(min(i-1,min(len(dist_list),Population.shape[1]))):
				if break_flag:
					break
				#k will be a selected individual from the distance array
				k 		= int(dist_list[j,1])
				
				#Now lets see if they belong to the same valley
				ind 	= sort_pop[i,:Population.shape[1]]
				k_ind	= sort_pop[k,:Population.shape[1]]
				
				# #Lets see if k_ind has already been assigned to a cluster
				# for cluster in cluster_dict.keys():
					# if k in cluster_dict[cluster]:
						# k 		= cluster
						# k_ind 	= sort_pop[k,:Population.shape[1]]
				for cluster in cluster_dict.keys():
					if k in cluster_dict[cluster]:
						HVFlag = Routines.Hill_Valley_Test(Prob, k_ind, ind, Args=Args, Points=5)
						if HVFlag:
							cluster_dict[cluster].append(i)
							break_flag = True
				
				# HVFlag = Routines.Hill_Valley_Test(Prob, k_ind, ind, Args=Args, Points=20)
				# if HVFlag:
					# for cluster in cluster_dict.keys():
						# if k in cluster_dict[cluster]:
							# cluster_dict[cluster].append(i)
					# break_flag = True
					#cluster_dict[k].append(i)
					#break_flag = True
			
			if not HVFlag:
				cluster_dict[i] = [i]
					
		return cluster_dict, sort_pop
			
			
			# #Now we wanna go through 
			# if i == 1:
				# k = dist_list[j]
				# for cluster in cluster_dict.keys():
					# flag = Routines.Hill_Valley_Test(Prob, sort_pop[cluster,:Population.shape[1]], sort_pop[i,:Population.shape[1]], Args=Args)
					# if flag:
						# cluster_dict[cluster].append(i)
			# else:
				# for j in range(min(i-1,Population.shape[1])):
					
		
		# for i in range(1,len(Population)-1):
			# flag = False
			# dist_list = []
			# for j in range(i):
				# dist_list.append([np.linalg.norm(sort_pop[i,:Population.shape[1]] - sort_pop[j,:Population.shape[1]]),j])
			# dist_list = np.asarray(dist_list)
			# dist_list = dist_list[dist_list[:,0].argsort()]

			# if i == 1:
				# k = dist_list[j]
				# for cluster in cluster_dict.keys():
					# flag = Routines.Hill_Valley_Test(Prob, sort_pop[cluster,:Population.shape[1]], sort_pop[i,:Population.shape[1]], Args=Args)
					# if flag:
						# cluster_dict[cluster].append(i)
			# else:
				# for j in range(0,min(i-1,Population.shape[1])):
					# if flag:
						# break
					# k = dist_list[j]
					
					# #Available Clusters
					# avail_clusters = cluster_dict.keys()
					# for cluster in avail_clusters:
						# flag = Routines.Hill_Valley_Test(Prob, sort_pop[cluster,:Population.shape[1]], sort_pop[i,:Population.shape[1]], Args=Args)
						# if flag:
							# cluster_dict[cluster].append(i)
							# break
			# if not flag:
				# cluster_dict[i] = [i]
						
		# return cluster_dict, sort_pop

	def Hill_Valley_Test(Prob, X, Y, Points=5, Args=''):
		#If return is True then the two candidate solutions are in the same valley.
		#If return is False then the two candidate solutions are in different valleys
		
		for i in range(1,Points+1):
			test_vec = X + (i / (Points + 1)) * (X - Y)
			if Args != '':
				if max(Prob(*np.concatenate((X,Args))),Prob(*np.concatenate((Y,Args)))) < Prob(*np.concatenate((test_vec,Args))):
					return False
			else:
				if max(Prob(*X),Prob(*Y)) < Prob(*test_vec):
					return False
		
		return True
		
class Optimization:
	
	def Hill_Valley_deGradient(Prob, Bounds, Hessian, Gradient, Max_Iters=5000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', benchmark='', args=''):
		print('Need to Implement')

	def Hill_Valley_Gradient(Prob, Bounds, Hessian, Gradient, Max_Iters=5000, tol=1e-6, popsize=50, benchmark='', args=''):
		print('Need to implement')

	def Hill_Valley_DE(Prob, Bounds, Max_Iters=5000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', benchmark='', args=''):
		print('Need to Implement')

	def BiSection_Method(Prob, Right, Left, tol=1e-6, Find_Value=0, Args=''):
		print('Need to Implement')
		
	def Information_Preserving_Differential_Evolution(prob, bounds, max_iters=5000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', benchmark='', args=''):
		
		#Check to make sure we can account for all of the parameters
		Routines.Check_Args(prob,args,bounds)
			
		pop_denorm	= Routines.Population_Setup(popsize, bounds)
		fitness		= Routines.Inital_Fitness(pop_denorm, args, prob)
		
		iter 			= 0
		boundLen		= len(bounds)
		error			= 1.0

		best_idx		= np.argmin(fitness)
		best			= pop_denorm[best_idx]
		best_ener		= fitness[best_idx]
		
		IP_list			= []
		
		while iter < max_iters and error > tol:
		
			if iter > 0:
				#Utilize the Information Preservation Stage
				#Sorted Array
				IP_array = np.asarray(IP_list)
				IP_array = IP_array[IP_array[:,boundLen].argsort()]
				IP_list = []
				pop_denorm = IP_array[:popsize,:boundLen]
				
			for j_idx in range(popsize):
				current = pop_denorm[j_idx]
				avail_idxs = [idx for idx in range(popsize) if idx != j_idx]
				
				#Mutation Strategy
				mutant = Routines.Strategy_Selection(pop_denorm, mutation, strategy, current, best, avail_idxs)
				
				#Crossover
				trial_vec = Routines.Crossover_Selection(boundLen, crosspop, current, mutant)
				
				if args != '':
					trial_fitness = prob(*np.concatenate((trial_vec,args)))
					current_fitness = prob(*np.concatenate((current, args)))
				else:
					trial_fitness = prob(*trial_vec)
					current_fitness = prob(*current)
					
				IP_list.append([*current, current_fitness])
				IP_list.append([*trial_vec, trial_fitness])
			
			IP_array 		= np.asarray(IP_list)
			new_best_idx 	= np.argmin(IP_array[:,boundLen])
			new_best 		= IP_array[new_best_idx,:boundLen]
			new_best_ener	= IP_array[new_best_idx,boundLen]
			
			if new_best_ener < best_ener:
				error = np.linalg.norm(best - new_best)
				best_ener = new_best_ener
				best = new_best
				best_idx = new_best_idx
				if error < tol:
					return "Optimal Solution", best, best_ener, iter

			iter += 1
			
		return "Non Optimal Solution", best, best_ener, iter
	
	def Coupled_Differential_Evolution(prob, bounds, Hessian, Derivative, max_iters=5000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin',benchmark='', mutScaling=False, newtonScale=1,newtonScaleSA=False, args=''):
		"""
		Agruments:
		
		Prob: A function to solve
		Bounds: The bounds of a particular problem, formatted as a list of lists
			e.g. [[min_x1, max_x1],[min_x2, max_x2],...[min_xn, max_xn]]
		Hessian: The Hessian Matrix of the given function should be of size n by n
		Derivative: Vector of length n, of derivatives of function
		
		Keyword Arguments:
		max_iters		: Maximum Iterations or Generations allowed
		tol				: Error Tolerance upon which meeting the algorithm will stop
		popsize			: Size of Population
		crosspop		: Probability of Cross Mutation
		mutation		: Scaling factor of vector differences
		strategy		: Mutation Strategy for developing the target vector
		benchmark		: Only necessary if you are benchmarking different strategies
							on unimodal optimization functions
		mutScaling		: Simulated annealing scaling of mutation parameter
		newtonScale		: Scaling of the Newton Improvement
		newtonScaleSA	: Simulated annealing scling of newtonScale parameter
		args			: Any additional agruments for your function, held constant throughout optimization
							give as list of args in the correct order they need to be applied.
		
		Strategy can play a role in the speed of your code. Here are the available choices
		rand1bin 		: a + m * (b-c) (Most generally used) (Default)
		rand2bin 		: a + m * (b - c + d - e)
		best1bin 		: best + m * (b - c)
		best2bin 		: best + m * (b - c + d - e)
		bestCurrent1bin : best + m * (b - current)
		bestCurrent2bin : best + m * (b - current + d - e)
		currentBest1bin	: current + m * ( best - current + b - c)
		currentBest2bin	: current + m * ( best - current + b - c + d - e)
		
		When performing simulated annealing, we first derive parameters U and K from a 
			given parameter (call it H), such that
					U = K and U = H/2
					
		U and K will not change through the optimization. To calculate some new H at iteration i, we apply the following,
					H_new = U + K * exp(-i/max_iters)
					
		Simulated Annealing should allow for us to not get into local extrema or saddle points
		"""
		
		#Check to make sure we can account for all of the parameters
		prob_parameters = prob.__code__.co_argcount
		if type(args) is list:
			args_count = len(args) + len(bounds)
		else:
			args_count = len(bounds)
			
		if prob_parameters != args_count:
			sys.exit('The number of parameters is not consistent. Quitting Optimization, please address in your problem statement.')
		
		if mutScaling:
			mutU = mutation/2
			mutK = mutU
		if newtonScaleSA:
			newtonU = newtonScale/2
			newtonK = newtonU
		
		Population		= np.random.rand(popsize,len(bounds))
		iter 			= 0
		boundLen		= len(bounds)
		
		error			= 1.0
		pop 			= np.random.rand(popsize, len(bounds))
		min_b, max_b 	= np.asarray(bounds).T
		diff 			= np.fabs(min_b - max_b)
		pop_denorm		= min_b + pop * diff
		if args != '':
			fitness			= np.asarray([prob(*np.concatenate((ind,args))) for ind in pop_denorm])
		else:
			fitness			= np.asarray([prob(*ind) for ind in pop_denorm])
		best_idx		= np.argmin(fitness)
		best			= pop_denorm[best_idx]
		best_ener		= fitness[best_idx]
		
		#Create Temp_Hessian Matrix
		
		Temp_Hessian = []
		for ih in range(len(Hessian)):
			temp_row = []
			for jh in range(len(Hessian[ih])):
				temp_row.append(1)
			Temp_Hessian.append(temp_row)
		
		Temp_Derivative = []
		for id in range(len(Derivative)):
			Temp_Derivative.append(1)
		
		while iter < max_iters and error > tol:
			if mutScaling:
				mutation = mutU + mutK * np.exp(-iter / max_iters)
			if newtonScaleSA:
				newtonScale = newtonU + newtonK * np.exp(-iter / max_iters)
				
			for j_idx in range(popsize):
				current = pop_denorm[j_idx]
				avail_idxs = [idx for idx in range(popsize) if idx != j_idx]
				
				#Mutation Strategy
				"""
				rand1bin 		: a + m * (b-c) (Most generally used) (Default)
				rand2bin 		: a + m * (b - c + d - e)
				best1bin 		: best + m * (b - c)
				best2bin 		: best + m * (b - c + d - e)
				bestCurrent1bin : best + m * (b - current)
				bestCurrent2bin : best + m * (b - current + d - e)
				currentBest1bin	: current + m * ( best - current + b - c)
				currentBest2bin	: current + m * ( best - current + b - c + d - e)
				"""
				
				if strategy is 'rand1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = a + mutation * (b - c)
				elif strategy is 'rand2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = a + mutation * (b + d - c - e)
				elif strategy is 'best1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = best + mutation * (b - c)
				elif strategy is 'best2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = best + mutation * (b + d - c - e)
				elif strategy is 'currentBest1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = current + mutation * (best - current + b - c)
				elif strategy is 'currentBest2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = current + mutation * (best - current + b + d - c - e)
				elif strategy is 'bestCurrent1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = best + mutation * (current - b)
				elif strategy is 'bestCurrent2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = best + mutation * (current + d - c - e)
				elif strategy is 'currentRand1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = current + mutation * (b - c)
				elif strategy is 'currentRand2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = current + mutation * (b - c + d - e)
				elif strategy is 'randCurrent1bin':
					a,b,c = pop_denorm[np.random.choice(avail_idxs,3,replace=False)]
					mutant = a + mutation * (current - c)
				elif strategy is 'randCurrent2bin':
					a,b,c,d,e = pop_denorm[np.random.choice(avail_idxs,5,replace=False)]
					mutant = a + (mutation * np.random.weibull(1/mutation)) * (current - c + d - e)
				
				#Crossover
				cross_points = np.random.rand(boundLen) < np.random.normal(crosspop,0.1)
				if not np.any(cross_points):
					cross_points[np.random.randint(0,boundLen)] = True
				trial_vec = (np.where(cross_points, mutant, current))
				
				#Newton's Improvement
				if args != '':
					for ih in range(len(Hessian)):
						for jh in range(len(Hessian[ih])):
							Temp_Hessian[ih][jh] = Hessian[ih][jh](*np.concatenate((trial_vec,args)))
						
					for id in range(len(Derivative)):
						Temp_Derivative[id] = -1 * Derivative[id](*np.concatenate((trial_vec,args)))
				else:
					for ih in range(len(Hessian)):
						for jh in range(len(Hessian[ih])):
							Temp_Hessian[ih][jh] = Hessian[ih][jh](*trial_vec)
						
					for id in range(len(Derivative)):
						Temp_Derivative[id] = -1 * Derivative[id](*trial_vec)
								
				vec_adjust = np.linalg.solve(Temp_Hessian, Temp_Derivative)
				
				trial_vec = trial_vec + newtonScale * vec_adjust
				trial_vec = np.abs(trial_vec)
				
				if args != '':
					trial_fitness = prob(*np.concatenate((trial_vec,args)))
				else:
					trial_fitness = prob(*trial_vec)
				
				if trial_fitness < fitness[j_idx]:
					fitness[j_idx] = trial_fitness
					pop_denorm[j_idx] = trial_vec
					if trial_fitness < best_ener:
						best_idx = j_idx
						if benchmark != '':
							error = np.linalg.norm(np.asarray(benchmark) - trial_vec)
						else:
							error = np.linalg.norm(best - trial_vec)
						best = trial_vec
						best_ener = trial_fitness
			
			iter += 1
		
		if error < tol:
			return "Optimal Solution", best, best_ener, iter
		else:
			return "Non Optimal Solution", best, best_ener, iter
		
	def Multimodal_Differential_Evolution(prob, bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin',benchmark='', mutScaling=False, args=''):

		#Check to make sure we can account for all of the parameters
		
		Routines.Check_Args(prob,args,bounds)
			
		pop_denorm	= Routines.Population_Setup(popsize, bounds)
		fitness		= Routines.Inital_Fitness(pop_denorm, args, prob)
		
		iter 			= 0
		boundLen		= len(bounds)
		error			= 1.0

		best_idx		= np.argmin(fitness)
		best			= pop_denorm[best_idx]
		best_ener		= fitness[best_idx]
		
		roots			= []
		
		if mutScaling:
			mutU = mutation/2
			mutK = mutU
		
		while iter < max_iters and error > tol:
		
			neigh_pop 		= []
			distance		= []
			idx_avail		= []
			for ind in range(popsize):
				temp_dist 	= []
				temp_pop	= []
				temp_avail 	= []
				for nind in range(popsize):
					if ind != nind:
						temp_dist.append(np.linalg.norm(pop_denorm[nind] - pop_denorm[ind]))
						temp_pop.append(pop_denorm[nind])
						temp_avail.append(nind)
				distance.append(temp_dist)
				neigh_pop.append(temp_pop)
				idx_avail.append(temp_avail)
			
			for j_idx in range(popsize):
				ind = pop_denorm[j_idx]
				neighbors = neigh_pop[j_idx]
				j_distance = distance[j_idx]
				nn_ind = neighbors[j_distance.index(min(j_distance))]
				avail_idxs = idx_avail[j_idx]

				
				#Mutation Strategy
				mutant = Routines.Multimodal_Strategy_Selection(pop_denorm, mutation, strategy, nn_ind, best, avail_idxs)
					
				#Crossover
				trial_vec = Routines.Crossover_Selection(boundLen, crosspop, ind, mutant)
				
				#Fitness evaluation
				
				trial_fitness = Routines.Calculate_Fitness(trial_vec, prob, args)
				
				if trial_fitness < fitness[j_idx]:
					fitness[j_idx] = trial_fitness
					pop_denorm[j_idx] = trial_vec
					if trial_fitness < best_ener:
						best_idx = j_idx
				
				#Root Collecting
				derror = 0.0
				dTol = 1e-6
				adjust_matrix = np.zeros((boundLen,boundLen))
				for i in range(boundLen):
					adjust_matrix[i,i] = 1e-10
				d_trial = trial_vec + np.matmul(adjust_matrix,trial_vec)
				adjust_matrix[i,i] = 0
				if args != '':
					derror += abs((prob(*np.concatenate((d_trial,args))) - prob(*np.concatenate((trial_vec,args)))) / (np.linalg.norm(d_trial - trial_vec)))
				else:
					derror += abs((prob(*d_trial) - prob(*trial_vec)) / (np.linalg.norm(d_trial - trial_vec)))
						
				if derror < dTol:
					add_flag = 1
					if len(roots) > 0:
						for root in roots:
							root_error = np.linalg.norm(root - trial_vec)
							if root_error < 1e-1:
								trial_vec = Routines.Repopulate_Vector(bounds)
								trial_fitness = Routines.Calculate_Fitness(trial_vec, prob, args)
								fitness[j_idx] 		= trial_fitness
								pop_denorm[j_idx]	= trial_vec
								#Root is already in the root list, so repopulate the vector.
								#Calculate the fitness and set inside the necessary array
								#Need to create repopulate function
								add_flag = 0
					if add_flag == 1:
						#Root is not in the root list
						print('Adding Root: {}'.format(trial_vec))
						roots.append(trial_vec)
						trial_vec = Routines.Repopulate_Vector(bounds)
						trial_fitness = Routines.Calculate_Fitness(trial_vec, prob, args)
						fitness[j_idx] 		= trial_fitness
						pop_denorm[j_idx]	= trial_vec
						add_flag = 0
						if len(roots) == benchmark:
							print('We have found all of the roots.')
							print('Total Number of Function Evaluations was {}.'.format(iter * popsize + j_idx))
							for root in roots:
								print('Root: {}'.format(root))
								print('Quitting the Evolution')
								return roots, (iter * popsize + j_idx), iter			
				#Fitness Comparison
			
			iter += 1
			
		return 'Not Finished Yet but found these roots  {}'.format(roots), (iter * popsize), iter
		
	def Multimodal_Coupled_Differential_Evolution(prob, bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin', benchmark=''):
		
		print('Need to implement')

	def Constrained_Differential_Evolution(Prob, Bounds, Constraints, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin',args=''):
		
		#Constraint Info is a dictionary containing the type of constraint and the Constraint Value.
		#All constraints are normalized to 0 on the RHS and an inequality
		
		Routines.Check_Args(Prob, args, Bounds)
		
		pop_denorm	= Routines.Population_Setup(popsize, Bounds)
		fitness		= Routines.Inital_Fitness(pop_denorm, args, Prob)
		
		iter		= 0
		boundLen	= len(Bounds)
		error		= 1.0
		
		penalty		= np.zeros((popsize,1))
		for con in Constraints:
			for idx, iidx in zip(pop_denorm,range(popsize)):
				penalty[iidx] += con(*idx)
		for idx in range(popsize):
			penalty[idx] = penalty[idx] / len(Constraints)
		
		best_idx	= np.argmin(fitness)
		best		= pop_denorm[best_idx]
		best_ener	= fitness[best_idx]
		best_pen	= penalty[best_idx]
		
		while iter < max_iters and error > tol:
			
			for idx in range(popsize):
				current		= pop_denorm[idx]
				avail_idxs	= [j_idx for j_idx in range(popsize) if j_idx != idx]
				
				mutant		= Routines.Strategy_Selection(pop_denorm, mutation, strategy, current, best, avail_idxs)
				
				mutant = Routines.Clip_Mutant(mutant, Bounds)
				
				trial_vec	= Routines.Crossover_Selection(boundLen, crosspop, current, mutant)
				
				current_fitness		= fitness[idx]
				trial_fitness 		= Routines.Calculate_Fitness(trial_vec, Prob, Args=args)
				
				current_penalty		= (float(penalty[idx]))
				trial_penalty		= 0
				for con in Constraints:
					trial_penalty	+= abs(Routines.Calculate_Fitness(trial_vec, con, Args=args))
				trial_penalty 		= trial_penalty / len(Constraints)
				
				#print('Current {} : Fitness {} : Penalty {}'.format(current, current_fitness, current_penalty))
				#print('Trial {} : Fitness {} : Penalty {}'.format(trial_vec, trial_fitness, trial_penalty))
				
				#Time to check the constraints
				if current_penalty <= 0 and trial_penalty <= 0:
					if trial_fitness < current_fitness:
						pop_denorm[idx] = trial_vec
						fitness[idx]	= trial_fitness
						current_fitness = trial_fitness
						current 		= trial_vec
						current_penalty = trial_penalty
				elif abs(current_penalty - trial_penalty) < tol:
					if trial_fitness < current_fitness:
						pop_denorm[idx] = trial_vec
						fitness[idx]	= trial_fitness
						current_fitness = trial_fitness
						current 		= trial_vec
						current_penalty = trial_penalty
				elif trial_penalty < current_penalty:
					pop_denorm[idx] = trial_vec
					fitness[idx]	= trial_fitness
					current_fitness = trial_fitness
					current 		= trial_vec
					current_penalty = trial_penalty
				
				#print('Keep Vec {}'.format(pop_denorm[idx]))
				
				if current_penalty < 0 and best_pen < 0:
					if current_fitness < best_ener:
						best_ener 	= current_fitness
						best_idx	= idx
						best		= current
						best_pen	= current_penalty
						print('1',best_ener, best, best_pen)
				elif abs(current_penalty - best_pen) < tol:
					if current_fitness < best_ener:
						best_ener 	= current_fitness
						best_idx	= idx
						best		= current
						best_pen	= current_penalty
						print('2',best_ener, best, best_pen)
				elif current_penalty < best_pen:
					best_ener 	= current_fitness
					best_idx	= idx
					best		= current
					best_pen	= current_penalty 
					print('3',best_ener, best, best_pen)
				
				# if current_fitness < best_ener:
					# best_ener = current_fitness
					# best_idx = idx
					# best = current
				
	
	def Differential_Evolution(prob, bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='rand1bin',benchmark='', mutScaling=False, args=''):

		"""
		Agruments:
		
		Prob: A function to solve
		Bounds: The bounds of a particular problem, formatted as a list of lists
			e.g. [[min_x1, max_x1],[min_x2, max_x2],...[min_xn, max_xn]]
		
		Keyword Arguments:
		max_iters		: Maximum Iterations or Generations allowed
		tol				: Error Tolerance upon which meeting the algorithm will stop
		popsize			: Size of Population
		crosspop		: Probability of Cross Mutation
		mutation		: Scaling factor of vector differences
		strategy		: Mutation Strategy for developing the target vector
		benchmark		: Only necessary if you are benchmarking different strategies
							on unimodal optimization functions
		mutScaling		: Simulated annealing scaling of mutation parameter
		args			: Any additional agruments for your function, held constant throughout optimization
							give as list of args in the correct order they need to be applied.
		
		Strategy can play a role in the speed of your code. Here are the available choices
		rand1bin 		: a + m * (b-c) (Most generally used) (Default)
		rand2bin 		: a + m * (b - c + d - e)
		best1bin 		: best + m * (b - c)
		best2bin 		: best + m * (b - c + d - e)
		bestCurrent1bin : best + m * (b - current)
		bestCurrent2bin : best + m * (b - current + d - e)
		currentBest1bin	: current + m * ( best - current + b - c) (Not Implemented)
		currentBest2bin	: current + m * ( best - current + b - c + d - e) (Not Implemented)
		
		When performing simulated annealing, we first derive parameters U and K from a 
			given parameter (call it H), such that
					U = K and U = H/2
					
		U and K will not change through the optimization. To calculate some new H at iteration i, we apply the following,
					H_new = U + K * exp(-i/max_iters)
					
		Simulated Annealing should allow for us to not get into local extrema or saddle points
		"""
		
		#Check to make sure we can account for all of the parameters
		Routines.Check_Args(prob,args,bounds)
		
		if mutScaling:
			mutU = mutation/2
			mutK = mutU
			
		pop_denorm	= Routines.Population_Setup(popsize, bounds)
		fitness		= Routines.Inital_Fitness(pop_denorm, args, prob)
		
		iter 			= 0
		boundLen		= len(bounds)
		error			= 1.0

		best_idx		= np.argmin(fitness)
		best			= pop_denorm[best_idx]
		best_ener		= fitness[best_idx]
		
		while iter < max_iters and error > tol:
			if mutScaling:
				mutation = mutU + mutK * np.exp(-iter / max_iters)
				
			for j_idx in range(popsize):
				current = pop_denorm[j_idx]
				avail_idxs = [idx for idx in range(popsize) if idx != j_idx]
				
				#Mutation Strategy
				mutant = Routines.Strategy_Selection(pop_denorm, mutation, strategy, current, best, avail_idxs)
				
				#Crossover
				trial_vec = Routines.Crossover_Selection(boundLen, crosspop, current, mutant)
				
				if args != '':
					trial_fitness = prob(*np.concatenate((trial_vec,args)))
				else:
					trial_fitness = prob(*trial_vec)
				
				if trial_fitness < fitness[j_idx]:
					fitness[j_idx] = trial_fitness
					pop_denorm[j_idx] = trial_vec
					if trial_fitness < best_ener:
						best_idx = j_idx
						if benchmark != '':
							error = np.linalg.norm(np.asarray(benchmark) - trial_vec)
						else:
							error = np.linalg.norm(trial_vec - best)
						best = trial_vec
						best_ener = trial_fitness
						if error < tol:
							return "Optimal Solution", best, best_ener, iter
			
			iter += 1
			
		return "Non Optimal Solution", best, best_ener, iter
	
	def Lemher_Mean(Set):
		
		numer = 0
		denom = 0
		for var in Set:
			numer += var**2
			denom += var
		
		return numer/denom
	
	def JADE_Differential_Evolution(prob, bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.5, mutation=0.5, benchmark='', args='', partition=0.25):
	
		#Check to make sure we can account for all of the parameters
		prob_parameters = prob.__code__.co_argcount
		if type(args) is list:
			args_count = len(args) + len(bounds)
		else:
			args_count = len(bounds)
			
		if prob_parameters != args_count:
			sys.exit('The number of parameters is not consistent. Quitting Optimization, please address in your problem statement.')
			
		Population		= np.random.rand(popsize,len(bounds))
		iter 			= 0
		boundLen		= len(bounds)
		
		error			= 1.0
		pop 			= np.random.rand(popsize, len(bounds))
		min_b, max_b 	= np.asarray(bounds).T
		diff 			= np.fabs(min_b - max_b)
		pop_denorm		= min_b + pop * diff
		
		archive_set = []
		
		if args != '':
			fitness			= np.asarray([prob(*np.concatenate((ind,args))) for ind in pop_denorm])
		else:
			fitness			= np.asarray([prob(*ind) for ind in pop_denorm])
		best_idx		= np.argmin(fitness)
		best			= pop_denorm[best_idx]
		best_ener		= fitness[best_idx]
		
		while iter < max_iters and error > tol:
			set_crossover	= []
			set_mutation	= []
			
			for j_idx in range(popsize):
				j_cross = np.clip(np.random.normal(crosspop, 0.1),0,1)
				j_mut 	= np.random.normal(mutation, 0.1)
				j_best	= pop_denorm[np.argpartition(fitness,int(partition*popsize))[np.random.randint(int(partition*popsize))]]
				current = pop_denorm[j_idx]
				avail_idxs = [idx for idx in range(popsize) if idx != j_idx]
				
				a,b = pop_denorm[np.random.choice(avail_idxs,2,replace=False)]
				mutant = current + j_mut * (j_best - current) + j_mut * (a - b)
				
				#Crossover
				cross_points = np.random.rand(boundLen) < j_cross
				if not np.any(cross_points):
					cross_points[np.random.randint(0,boundLen)] = True
				trial_vec = (np.where(cross_points, mutant, current))
				
				if args != '':
					trial_fitness = prob(*np.concatenate((trial_vec,args)))
				else:
					trial_fitness = prob(*trial_vec)
					
				if fitness[j_idx] < trial_fitness:
					#We keep the original vector
					fitness[j_idx] = fitness[j_idx]
				else:
					#We will replace it
					archive_set.append(current)
					pop_denorm[j_idx]	= trial_vec
					set_crossover.append(j_cross)
					set_mutation.append(j_mut)
					if trial_fitness < best_ener:
						best_idx = j_idx
						if benchmark != '':
							error = np.linalg.norm(np.asarray(benchmark) - trial_vec)
						else:
							error = np.linalg.norm(best - trial_vec)
						best = trial_vec
						best_ener = trial_fitness
						if error < tol:
							return "Optimal Solution", best, best_ener
							
			#calculate the new crosspop and mutation parameters
			c = 0.25
			crosspop = (1-c) * crosspop + c * np.mean(np.asarray(set_crossover))
			mutation =  (1-c) * mutation + c * Optimization.Lemher_Mean(set_mutation)
			iter += 1
		
		if error < tol:
			return "Optimal Solution", best, best_ener
		else:
			return "Non Optimal Solution", best, best_ener