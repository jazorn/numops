"""
Numerical Optimization Library
Written by Jacob Zorn
Date Created: 11/5/2019
Date Updated: 1/27/2020
Version: 0.2.0
"""

#Import Outside Libraries
import numpy as np
import sys
import numdifftools as nd
import matplotlib.pyplot as plt
import itertools as itools
import sympy as sp
from scipy.sparse import linalg
from scipy.stats import cauchy as sciCauchy

#-------------------------------

#Classes

class General_Routines:

	def Check_Args(Prob, Args, Bounds):

		if type(Args) is list:
			args_count = len(Args) + len(Bounds)
		else:
			args_count = len(Bounds)

		prob_parameters = Prob.__code__.co_argcount

		if prob_parameters != args_count:
			sys.exit('The number of parameters is not consistent. Quitting Optimization, please address in your problem statement.')

	def Create_Vector(Bounds):

		min_b, max_b 	= np.asarray(Bounds).T
		return (min_b + np.random.rand(1,len(Bounds)) * np.fabs(min_b - max_b))[0]

	def Calculate_Fitness(Vector, Prob, Args=''):

		if Args == '':
			return Prob(*Vector)
		else:
			return Prob(*np.concatenate((Vector,Args)))

	def Population_Setup(Popsize, Bounds):

		Population		= np.random.rand(Popsize, len(Bounds))
		min_b, max_b	= np.asarray(Bounds).T
		return min_b + Population * np.fabs(min_b - max_b)

	def Elitest_Selection(fV1, fV2):

		if fV1 <= fV2:
			return True
		else:
			return False

	def Strategy_Selection(Population, Mutation, Current, Best, avail_idxs, objFunc, Strategy='rand1bin'):

		#If performing Multimodal, choose currentRand1bin, currentRand2bin, best1bin, best2bin, and replace either best or current with the nearest neighbor

		a,b,c,d,e = Population[np.random.choice(avail_idxs, 5, replace=False)]

		if Strategy == 'rand1bin':
			Mutant = a + Mutation * (b-c)
		elif Strategy == 'rand2bin':
			Mutant = a + Mutation * (b - c + d - e)
		elif Strategy == 'best1bin':
			Mutant = Best + Mutation * (b - c)
		elif Strategy == 'best2bin':
			Mutant = Best + Mutation * (b - c + d - e)
		elif Strategy == 'currentBest1bin':
			Mutant = Current + Mutation * (Best - Current + b - c)
		elif Strategy == 'currentBest2bin':
			Mutant = Current + Mutation * (Best - Current + b - c + d - e)
		elif Strategy == 'bestCurrent1bin':
			Mutant = Best + Mutation * (Current - b)
		elif Strategy == 'bestCurrent2bin':
			Mutant = Best + Mutation * (Current - b + d - e)
		elif Strategy == 'currentRand1bin':
			Mutant = Current + Mutation * (b - c)
		elif Strategy == 'currentRand2bin':
			Mutant = Current + Mutation * (b - c + d - e)
		elif Strategy == 'randCurrent1bin':
			Mutant = a + Mutation * (Current - c)
		elif Strategy == 'randCurrent2bin':
			Mutant = a + Mutation * (Current - c + d - e)
		elif Strategy == 'triangle':
			pPrime = abs(objFunc(*a)) + abs(objFunc(*b)) + abs(objFunc(*c))
			aTerm = (a + b + c)/3
			bTerm = ((abs(objFunc(*b))/pPrime) - ((abs(objFunc(*a))/pPrime))) * (a - b)
			bTerm += ((abs(objFunc(*c))/pPrime) - ((abs(objFunc(*b))/pPrime))) * (b - c)
			bTerm += ((abs(objFunc(*a))/pPrime) - ((abs(objFunc(*c))/pPrime))) * (c - a)
			Mutant = aTerm + bTerm

		return Mutant

	def Mutant_Crossover(CrossPollinator, Current, Mutant, Bounds):

		for i in range(len(Bounds)):
			Mutant[i] = np.clip(Mutant[i],Bounds[i][0],Bounds[i][1])

		cross_points	= np.random.rand(len(Bounds)) < CrossPollinator
		cross_points[np.random.randint(0,len(Bounds))] = True
		Trial = np.where(cross_points, Mutant, Current)
		return Trial

	def Gradient_Assist(Gradient, Hessian, Scaling, Vector, Bounds, Args=''):

		if Args == '':
			array_Hessian = np.asarray(Hessian(*Vector))
			array_Gradient = np.asarray(Gradient(*Vector))
		else:
			array_Hessian = np.asarray(Hessian(*Vector,*Args))
			array_Gradient = np.asarray(Gradient(*Vector, *Args))

		try:
			# vec_adjust	= linalg.cgs(array_Hessian, -1 * array_Gradient)[0]
			vec_adjust 	= np.linalg.solve(array_Hessian, -1 * array_Gradient)
		except:
			vec_adjust	= np.zeros(array_Gradient.shape)

		Trial = Vector + Scaling * vec_adjust.T[0]
		for i in range(len(Bounds)):
			Trial[i] = np.clip(Trial[i],Bounds[i][0],Bounds[i][1])

		return Trial

	def Preserve_Information(DoublePopulation, DoubleFitness):

		#The Population and the Fitness should have the same indexes
		newPop = DoublePopulation[DoubleFitness.argsort()[:int(DoublePopulation.shape[0]/2)]]
		newFit = DoubleFitness[DoubleFitness.argsort()[:int(DoublePopulation.shape[0]/2)]]
		return newPop, newFit

	def Calculate_Distance(Current, Population):
		NeighDist	= 100
		for idx in Population:
			dist 	= np.linalg.norm(Current - idx)
			if dist < NeighDist and dist != 0:
				NearNeigh = idx
				NeighDist = dist

		return NearNeigh

	def Fitness_Test(Fitness1, Fitness2):

		if (Fitness1 <= Fitness2):
			return True
		else:
			return False

	def Constraint_Test(Fitness1, Fitness2, Penalty1, Penalty2, Epsilon):
		#Return True if Replacing Fitness2 with Fitness1

		if (Penalty1 < Epsilon and Penalty2 < Epsilon):
			return General_Routines.Fitness_Test(Fitness1, Fitness2)
		elif (Penalty1 == Penalty2):
			return General_Routines.Fitness_Test(Fitness1, Fitness2)
		elif (Penalty1 < Penalty2):
			return True
		else:
			return False

	def Penalty_Elitest_Archive(Population, Penalty, Portion):
		sorted_Pen = Penalty.argsort()
		Archive = []
		for i in range(round(len(Population)*Portion)):
			Archive.append(Population[sorted_Pen[i]].tolist())

		return np.asarray(Archive)

	def Normalize_Penalty(arrPenalty):

		conMaxs		= arrPenalty.max(axis=0)
		conMaxs		= np.where(conMaxs < 1e-12, 1e-12, conMaxs)
		maxCons 	= 1 / arrPenalty.max(axis=0)
		sumMaxCons 	= maxCons.sum()

		arrPenalty 	= arrPenalty * maxCons
		arrPenalty 	= arrPenalty.sum(1)
		arrPenalty 	= arrPenalty / sumMaxCons

		return arrPenalty, conMaxs

	def Calculate_Penalty(Vector, Con, Args=''):
		conValue	= General_Routines.Calculate_Fitness(Vector, Con, Args=Args)
		if (abs(conValue) - 0.00001) > 0:
			return abs(conValue)
		else:
			return 0

	def Calculate_Penalty_ineq(Vector, Con, Args=''):
		conValue	= General_Routines.Calculate_Fitness(Vector, Con, Args=Args)
		if conValue <= 0:
			return 0
		else:
			return abs(conValue)

	def Probability_SA(Fit1, Fit2, Temp):
		if Fit2 < Fit1:
			return 1.0
		else:
			return np.exp((Fit1 - Fit2)/Temp)

	def SQG_Strategy(Population, Mutation, Best, avail_idxs, objFunc, pairs=2, Args=''):

		sqgVectors	= Population[np.random.choice(avail_idxs, int(pairs*2), replace=False)]

		fits = np.asarray([General_Routines.Calculate_Fitness(vec, objFunc, Args=Args) for vec in sqgVectors])

		scaleNumer	= np.zeros(Best.shape)
		for i in range(0,pairs*2,2):
			scaleNumer	+= (sqgVectors[i] - sqgVectors[i+1])
		scaleNumer	= (1/pairs) * np.linalg.norm(scaleNumer)

		scaleDenom = np.zeros(Best.shape)
		for i in range(0,pairs*2,2):
			scaleDenom	+= ((fits[i] - fits[i-1])/np.linalg.norm(sqgVectors[i] - sqgVectors[i+1])) * (sqgVectors[i] - sqgVectors[i+1])
		scaleDenom	= np.linalg.norm(scaleDenom)
			# scaleDenom	= np.linalg.norm(((fits[i] - fits[i+1])/(np.linalg.norm(sqgVectors[i] - sqgVectors[i+1]))) * (sqgVectors[i] - sqgVectors[i+1]))

		scaleFactor	= scaleNumer / scaleDenom

		vectorDifference	= np.zeros(Best.shape)
		for i in range(0, pairs*2, 2):
			vectorDifference	+= ((fits[i] - fits[i+1])/np.linalg.norm(sqgVectors[i] - sqgVectors[i+1])) * (sqgVectors[i] - sqgVectors[i+1])

		return Best - Mutation * scaleFactor * vectorDifference

class Differential_Evolution:

	def SQG_DE(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.5, mutation=0.5, Args='', benchmark='', pairs=2):
		#From SQG-DifferentialEvolution for Difficult Optimization problems under a tight function evalutation budget
		#International Workshop on Machine Learning, Optimization, and Big Data. Springer 2017


		General_Routines.Check_Args(Prob, Args, Bounds)

		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])

		iteration 		= 0
		error			= 1.0

		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]

		while iteration < max_iters and error > tol:
			for idx in range(popsize):
				Current 	= Population[idx]
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx]

				#Create Mutant Vecotr
				Mutant 		= General_Routines.SQG_Strategy(Population, mutation, Best, avail_idxs, Prob, pairs=pairs, Args=Args)

				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)

				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)

				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])

				if flag_Elite:
					Fitness[idx]	= Trial_Fitness
					Population[idx]	= Trial_Vec
					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						if benchmark != '':
							error = abs(Trial_Fitness - benchmark)/abs(benchmark)
						else:
							error 		= np.linalg.norm(Trial_Vec - Best)
							#error		= abs(Trial_Fitness - Best_Fit)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness
						if error < tol:
							return 'Optimal Solution', Best, Best_Fit, iteration+1

			iteration += 1

		return 'Non-Optimal Solution', Best, Best_Fit, iteration + 1

	def JADE(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crossover=0.5, mutation=0.5, Args='', benchmark='', psize=0.10, flagIP=False, flagGrad=False, gradDeriv='', gradHessian='', gradScale=1.0):

		General_Routines.Check_Args(Prob, Args, Bounds)

		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])

		iteration 		= 0
		error			= 1.0

		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]

		if flagIP:
			dPop = np.zeros((2*popsize,len(Bounds)))
			dFit = np.zeros((2*popsize))

		#Implement Later
		# Archive			= np.zeros((popsize, Bounds))

		while iteration < max_iters and error > tol:

			if iteration > 0 and flagIP:
				Population, Fitness = General_Routines.Preserve_Information(dPop, dFit)

			#Mutation Factors
			mutationArray 	= []

			#Crossover Factors
			crossoverArray = []

			for idx in range(popsize):

				currentMut 	= sciCauchy.rvs(loc=mutation, scale=0.1)
				while currentMut > 1 or currentMut <= 0:
					currentMut = sciCauchy.rvs(loc=mutation, scale=0.1)

				currentCross	= np.random.normal(loc=crossover, scale=0.1)
				while currentCross > 1 or currentCross <= 0:
					currentCross	= np.random.normal(loc=crossover, scale=0.1)

				Current 	= Population[idx]
				pBestIDX	= np.random.choice(Fitness.argsort()[:int(popsize*psize)])
				pBest		= Population[pBestIDX]
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx and jidx != pBestIDX]

				#Mutation Strategy
				Mutant 			= General_Routines.Strategy_Selection(Population, currentMut, Current, pBest, avail_idxs, Prob, Strategy='currentBest1bin')

				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(currentCross, Current, Mutant, Bounds)

				#Gradient Assist
				if flagGrad:
					Trial_Vec = General_Routines.Gradient_Assist(gradDeriv, gradHessian, gradScale, Trial_Vec, Bounds, Args=Args)

				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)

				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])
				#True means replace Trial_Vec with Current_Vec

				if flagIP:
					dPop[idx,:] 					= Current
					dPop[(2*popsize - idx -1),:] 	= Trial_Vec
					dFit[idx] 						= Fitness[idx]
					dFit[(2*popsize - 1 - idx)]		= Trial_Fitness
					crossoverArray.append(currentCross)
					mutationArray.append(currentMut)

					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						if benchmark != '':
							error = abs(Trial_Fitness - benchmark)/abs(benchmark)
						else:
							error 		= np.linalg.norm(Trial_Vec - Best)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness
						if error < tol:
							return 'Optimal Solution', Best, Best_Fit, iteration+1

				if not flagIP:
					flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])
					#True means replace Trial_Vec with Current_Vec
					if flag_Elite:
						Fitness[idx]	= Trial_Fitness
						Population[idx]	= Trial_Vec
						crossoverArray.append(currentCross)
						mutationArray.append(currentMut)
						flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
						if flag_Best:
							Best_IDX	= idx
							if benchmark != '':
								error = abs(Trial_Fitness - benchmark)/abs(benchmark)
							else:
								error 		= np.linalg.norm(Trial_Vec - Best)
								#error		= abs(Trial_Fitness - Best_Fit)
							Best		= Trial_Vec
							Best_Fit	= Trial_Fitness
							if error < tol:
								return 'Optimal Solution', Best, Best_Fit, iteration+1

				# if flag_Elite:
				# 	Fitness[idx]	= Trial_Fitness
				# 	Population[idx]	= Trial_Vec
				# 	crossoverArray.append(currentCross)
				# 	mutationArray.append(currentMut)
				# 	flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
				# 	if flag_Best:
				# 		Best_IDX	= idx
				# 		if benchmark != '':
				# 			error = abs(Trial_Fitness - benchmark)/abs(benchmark)
				# 		else:
				# 			error 		= np.linalg.norm(Trial_Vec - Best)
				# 			#error		= abs(Trial_Fitness - Best_Fit)
				# 		Best		= Trial_Vec
				# 		Best_Fit	= Trial_Fitness
				# 		if error < tol:
				# 			return 'Optimal Solution', Best, Best_Fit, iteration+1

			#Update Crossover
			crossover = (1 - 0.1) * crossover + 0.1 * np.asarray(crossoverArray).mean()

			#Update Mutation
			mutation = (1 - 0.1) * mutation + 0.1 * (((np.asarray(mutationArray))**2).sum() / np.asarray(mutationArray).sum())

			iteration += 1

		return 'Non-Optimal Solution', Best, Best_Fit, iteration + 1

	def Differential_Evolution(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, gradScale=1.0, strategy='rand1bin', Args='', flagIP=False, flagGrad=False, gradDeriv='', gradHessian='', benchmark=''):

		General_Routines.Check_Args(Prob, Args, Bounds)

		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])

		iteration 		= 0
		error			= 1.0

		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]

		if flagIP:
			dPop = np.zeros((2*popsize,len(Bounds)))
			dFit = np.zeros((2*popsize))

		while iteration < max_iters and error > tol:

			if iteration > 0 and flagIP:
				Population, Fitness = General_Routines.Preserve_Information(dPop, dFit)

			# if flagIP and not flagGrad:
			# 	np.savetxt('generationIP_{}.txt'.format(iteration),
			# 				Population, delimiter=',')
			# if flagGrad and not flagIP:
			# 	np.savetxt('generationGrad_{}.txt'.format(iteration),
			# 				Population, delimiter=',')
			# if flagGrad and flagIP:
			# 	np.savetxt('generationGradIP_{}.txt'.format(iteration),
			# 				Population, delimiter=',')
			# if not flagIP and not flagGrad:
			# 	np.savetxt('generation_{}.txt'.format(iteration),
			# 				Population, delimiter=',')

			for idx in range(popsize):
				Current 	= Population[idx]
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx]

				#Mutation Strategy
				Mutant 			= General_Routines.Strategy_Selection(Population, mutation, Current, Best, avail_idxs, Prob, Strategy=strategy)

				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)

				#Gradient Assist
				if flagGrad:
					Trial_Vec = General_Routines.Gradient_Assist(gradDeriv, gradHessian, gradScale, Trial_Vec, Bounds, Args=Args)

				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)

				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])

				if flagIP:
					dPop[idx,:] 					= Current
					dPop[(2*popsize - idx -1),:] 	= Trial_Vec
					dFit[idx] 						= Fitness[idx]
					dFit[(2*popsize - 1 - idx)]		= Trial_Fitness

					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						if benchmark != '':
							error = abs(Trial_Fitness - benchmark)/abs(benchmark)
						else:
							error 		= np.linalg.norm(Trial_Vec - Best)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness
						if error < tol:
							return 'Optimal Solution', Best, Best_Fit, iteration+1

				if not flagIP:
					flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])
					#True means replace Trial_Vec with Current_Vec
					if flag_Elite:
						Fitness[idx]	= Trial_Fitness
						Population[idx]	= Trial_Vec
						flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
						if flag_Best:
							Best_IDX	= idx
							if benchmark != '':
								error = abs(Trial_Fitness - benchmark)/abs(benchmark)
							else:
								error 		= np.linalg.norm(Trial_Vec - Best)
								#error		= abs(Trial_Fitness - Best_Fit)
							Best		= Trial_Vec
							Best_Fit	= Trial_Fitness
							if error < tol:
								return 'Optimal Solution', Best, Best_Fit, iteration+1

			iteration += 1

		return 'Non-Optimal Solution', Best, Best_Fit, iteration + 1

	def Multimodal_Differential_Evolution(Prob, Bounds, max_iters=2000, tol=1e-6, popsize=50, crosspop=0.8, mutation=0.6, strategy='currentRand1bin', Args='', Benchmark=''):

		General_Routines.Check_Args(Prob, Args, Bounds)

		Population		= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 		= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])

		iteration 		= 0
		error			= 1.0
		Roots			= []
		dTol			= 1e-6

		Best_IDX 		= np.argmin(Fitness)
		Best			= Population[Best_IDX]
		Best_Fit		= Fitness[Best_IDX]

		while iteration < max_iters:
			for idx in range(popsize):
				Current 	= Population[idx]
				NearNeigh	= General_Routines.Calculate_Distance(Current, Population)
				avail_idxs	= [jidx for jidx in range(popsize) if jidx != idx]

				#Mutation Strategy
				Mutant			= General_Routines.Strategy_Selection(Population, mutation, NearNeigh, Best, avail_idxs, Prob, Strategy=strategy)

				#Crossover
				Trial_Vec		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)

				#Fitness
				Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)

				#Comparsion
				flag_Elite		= General_Routines.Elitest_Selection(Trial_Fitness, Fitness[idx])

				#Replacement
				if flag_Elite:
					Fitness[idx]	= Trial_Fitness
					Population[idx]	= Trial_Vec
					flag_Best		= General_Routines.Elitest_Selection(Trial_Fitness, Best_Fit)
					if flag_Best:
						Best_IDX	= idx
						error		= np.abs(Trial_Fitness - Best)
						Best		= Trial_Vec
						Best_Fit	= Trial_Fitness

				#Root Collecting
				dError			= 0.0
				adjust_Matrix	= np.zeros((len(Bounds),len(Bounds)))
				for i in range(len(Bounds)):
					adjust_Matrix[i,i]	= 1e-10
					dTrial				= Trial_Vec + np.matmul(adjust_Matrix, Trial_Vec)
					adjust_Matrix[i,i] 	= 0.0
					dError				+= abs(General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args) - General_Routines.Calculate_Fitness(dTrial, Prob, Args=Args)) / np.linalg.norm(dTrial - Trial_Vec)

				if dError < dTol:
					flag_Add			= True
					if len(Roots) > 0:
						for root in Roots:
							error_Root		= np.linalg.norm(root - Trial_Vec)
							if error_Root < 1e-1:
								Trial_Vec		= General_Routines.Create_Vector(Bounds)
								Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
								Fitness[idx]	= Trial_Fitness
								Population[idx]	= Trial_Vec
								flag_Add		= False
					if flag_Add:
						print('Adding Root: {}'.format(Trial_Vec))
						Roots.append(Trial_Vec)
						Trial_Vec		= General_Routines.Create_Vector(Bounds)
						Trial_Fitness	= General_Routines.Calculate_Fitness(Trial_Vec, Prob, Args=Args)
						Fitness[idx]	= Trial_Fitness
						Population[idx]	= Trial_Vec
						flag_Add		= False
						if len(Roots) == Benchmark and Benchmark != '':
							return 'Found all roots', Roots

			iteration += 1

		return 'Reached Max Iterations', Roots

	def Constrained_Differential_Evolution(Prob, Bounds, Constraint_Dict, max_iters=2000, tol=1e-8, popsize=50, crosspop=0.6, mutation=0.7, strategy='rand1bin', Args=''):

		General_Routines.Check_Args(Prob, Args, Bounds)

		Relaxed_Gen			= round(0.5 * max_iters)
		Theta				= 0.30
		Regens				= 3
		iteration			= 0

		Population			= General_Routines.Population_Setup(popsize, Bounds)
		Fitness 			= np.asarray([General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population])
		#Penalty			= np.asarray([[General_Routines.Calculate_Fitness(idx, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']] for idx in Population])

		Penalty				= np.asarray([[General_Routines.Calculate_Penalty(idx, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']] for idx in Population])

		#Penalty_Scalar		= Penalty.sum(1)
		Penalty_Scalar, conMaxs	= General_Routines.Normalize_Penalty(Penalty)

		Elitest_Archive		= General_Routines.Penalty_Elitest_Archive(Population, Penalty_Scalar, Theta)

		eps0				= np.sort(Penalty_Scalar)[round(Theta * popsize)]
		eps					= eps0
		fError				= False

		while iteration < max_iters:
			for idx in range(popsize):
				Current		= Population[idx]
				if iteration < Relaxed_Gen:
					Pop_Arch	= np.concatenate((Population, Elitest_Archive),axis=0)
				else:
					Pop_Arch = Population

				avail_idxs	= [jidx for jidx in range(len(Pop_Arch)) if idx != jidx]

				Current_Fitness		= Fitness[idx]
				Current_Penalty		= Penalty_Scalar[idx]

				fGenerate	= True
				count_Gen	= 0

				while fGenerate and count_Gen < Regens:
					Mutant		= General_Routines.Strategy_Selection(Pop_Arch, mutation, Current, Current, avail_idxs, Prob, Strategy='rand1bin')
					Mutant		= General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)
					Mutant_Fit	= General_Routines.Calculate_Fitness(Mutant, Prob, Args=Args)
					#Mutant_Pen	= np.asarray([General_Routines.Calculate_Fitness(Mutant, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']])

					Mutant_Pen	= np.asarray([General_Routines.Calculate_Penalty(Mutant, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']])

					Mutant_Pen	= np.where(Mutant_Pen < 1e-12, 1e-12, Mutant_Pen)

					mutMaxs		= np.where(Mutant_Pen > conMaxs, Mutant_Pen, conMaxs)
					sumMutMax	= (1 / mutMaxs).sum()
					Mutant_Pen_Scalar	= Mutant_Pen * (1 / mutMaxs)
					Mutant_Pen_Scalar	= Mutant_Pen_Scalar.sum()
					Mutant_Pen_Scalar	= Mutant_Pen_Scalar / sumMutMax

					Mutant_Pen_Scalar = Mutant_Pen.sum()

					if Mutant_Pen_Scalar <= eps:
						fGenerate 	= False
					elif Mutant_Pen_Scalar <= Current_Penalty:
						fGenerate	= False
					count_Gen += 1

				fChange			= General_Routines.Constraint_Test(Mutant_Fit, Current_Fitness, Mutant_Pen_Scalar, Current_Penalty, eps)

				if fChange:
					Fitness[idx]		= Mutant_Fit
					Penalty[idx]		= Mutant_Pen
					Penalty_Scalar[idx]	= Mutant_Pen_Scalar
					Population[idx]		= Mutant
					#conMaxs				= mutMaxs
					if fError:
						if abs(bestFit - Mutant_Fit) / abs(bestFit) < tol:
							print('Within Tolerance of Optimal Solution')
							return Population, Fitness, Penalty_Scalar, Penalty


			#Penalty_Scalar, conMaxs	= General_Routines.Normalize_Penalty(Penalty)
			if iteration > Relaxed_Gen:
				eps					= np.sort(Penalty_Scalar)[round(Theta * popsize)]
			else:
				eps					= eps0 * (1 - iteration / Relaxed_Gen)**5
			if eps < 1e-12:
				eps = 1e-12
				fError 				= True
				bestFit 			= Fitness[Fitness.argsort()[0]]

			Elitest_Archive		= General_Routines.Penalty_Elitest_Archive(Population, Penalty_Scalar, Theta)

			iteration			+= 1

		return Population, Fitness, Penalty_Scalar, Penalty

	def Regression_Differential_Evolution(Prob, Bounds, TrainingVecs, TrainingYHats, max_iters=2000, tol=1e-6, popsize=50,crosspop=0.8, mutation=0.6, strategy='rand1bin', flagIP=False, flagGrad=False):

		Population = General_Routines.Population_Setup(popsize,Bounds)
		Fitness = np.asarray([((Prob(*ind,*TrainingVecs)-TrainingYHats)**2).sum() for ind in Population])
		Best_IDX = np.argmin(Fitness)
		Best = Population[Best_IDX]
		Best_Fit = Fitness[Best_IDX]
		iteration = 0
		error = 1.0

		while iteration < max_iters and error > tol:
			for idx in range(popsize):
				Current = Population[idx]
				avail_idxs = [jidx for jidx in range(popsize) if jidx != idx]

				#Mutation Strategy
				Mutant = General_Routines.Strategy_Selection(Population, mutation, Current, Best, avail_idxs, Prob, Strategy=strategy)

				#Crossover
				Trial_Vec = General_Routines.Mutant_Crossover(crosspop, Current, Mutant, Bounds)

				#Fitness
				trialFitness = ((Prob(*Trial_Vec,*TrainingVecs)-TrainingYHats)**2).sum()

				#Comparsion
				flag_Elite = General_Routines.Elitest_Selection(trialFitness, Fitness[idx])

				if flag_Elite:
					Fitness[idx] = trialFitness
					Population[idx] = Trial_Vec
					flag_Best = General_Routines.Elitest_Selection(trialFitness, Best_Fit)
					if flag_Best:
						Best_IDX = idx
						error = np.abs(trialFitness - Best_Fit)
						Best = Trial_Vec
						Best_Fit = trialFitness
						if error < tol:
							return 'Optimal Result', Best, Best_Fit, iteration+1

			iteration += 1
		return 'Non-Optimal Result', Best, Best_Fit, iteration+1

class ParticleSwarm:

	def classicalParticleSwarm(Prob, Bounds, max_iters=1000, tol=1e-6, popsize=50, Args=''):

		General_Routines.Check_Args(Prob, Args, Bounds)

		Population		= General_Routines.Population_Setup(popsize, Bounds)

		iteration 		= 0
		error			= 1.0

		Best			= []
		Best_Fit		= -1

		personalBestVectors	= np.asarray([idx for idx in Population])

		Velocity		= -1 + 2 * np.random.rand(popsize, len(Bounds))
		c1, c2			= 1.0, 2.0

		while iteration < max_iters and error > tol:
			Fitness = [General_Routines.Calculate_Fitness(idx, Prob, Args=Args) for idx in Population]

			if iteration == 0:
				personalBestFitness = [idx for idx in Fitness]

			for i in range(popsize):
				if Fitness[i] < personalBestFitness[i]:
					personalBestFitness[i] = Fitness[i]
					personalBestVectors[i,:] = Population[i,:]
				if Fitness[i] < Best_Fit or Best_Fit == -1:
					Best_Fit = Fitness[i]
					Best = Population[i,:]

			for i in range(popsize):
				newVelo = Velocity[i,:] * 0.5
				newVelo += 1.0 * (personalBestVectors[i,:] - Population[i,:])
				newVelo += 2.0 * (Best - Population[i,:])

				Population[i,:] += newVelo
				Velocity[i,:] = newVelo
				for j in range(len(Bounds)):
					if Population[i,j] < Bounds[j][0]:
						Population[i,j] = Bounds[j][0]
					elif Population[i,j] > Bounds[j][1]:
						Population[i,j] = Bounds[j][1]

			iteration += 1

		return Best_Fit, Best, iteration

class General_Optimization:

	def Broyden_Method(Prob):
		print('Need to implement')

	def Secant_Method(Prob, x1, x2, max_iters=2000, tol=1e-6, Args=''):

		if Args != '':
			fa = Prob(x1, *Args)
			fb = Prob(x2, *Args)
		else:
			fa = Prob(x1)
			fb = Prob(x2)

		error = 1
		while error > tol:
			x3 = x2 - fb * (x2 - x1) / (fb - fa)
			x1, x2 = x2, x3
			if Args != '':
				fa = Prob(x1, *Args)
				fb = Prob(x2, *Args)
			else:
				fa = Prob(x1)
				fb = Prob(x2)
			error = abs(fb)

		return x2

	def Bisection_Method(Prob, Right, Left, tol=1e-6, Find_Value=0, Args=''):


		if Args != '':
			fRight 	= Prob(Right,*Args) - Find_Value
			fLeft	= Prob(Left,*Args) - Find_Value

			if (fRight * fLeft) > 0:
				sys.exit('The Bounds do not meet the requirements for the Bisection Method. fRight: {} and fLeft: {}'.format(fRight, fLeft))

			Error	= 1.0
			while Error > tol:
				xNew	= (Right + Left) / 2
				fNew	= Prob(xNew,*Args)- Find_Value

				if fRight * fNew < 0:
					Left 	= xNew
					fLeft	= fNew
				else:
					Right	= xNew
					fRight	= fNew

				Error = abs(fNew)

			return xNew

		else:
			fRight 	= Prob(Right) - Find_Value
			fLeft	= Prob(Left) - Find_Value

			if (fRight * fLeft) > 0:
				sys.exit('The Bounds do not meet the requirements for the Bisection Method. fRight: {} and fLeft: {}'.format(fRight, fLeft))

			Error	= 1.0
			while Error > tol:
				xNew	= (Right + Left) / 2
				fNew	= Prob(xNew)- Find_Value

				if fRight * fNew < 0:
					Left 	= xNew
					fLeft	= fNew
				else:
					Right	= xNew
					fRight	= fNew

				Error = abs(fNew)

			return xNew

	def Newton_Raphson(Prob, Derivative, Inital, Find_Value, tol=1e-6, Args='', max_iters=2000):

		iteration 	= 0
		x			= Inital
		while iteration < max_iters:

			y 		= Prob(*x, *Args)
			yprime	= Derivative(*x, *Args)

			if abs(yprime) < 1e-14:
				break
				return x

			xNew	= x - y/yprime

			if abs(xNew - x) < tol:
				return xNew

			x		= xNew

		return x

	def Newton_Method(Function, Gradient, Hessian, Inital, tol=1e-6, max_iters=2000, Args=''):
		#These should all be lamdified Sympy expression

		error 		= 100
		iteration 	= 1

		if Args == '':
			while iteration < max_iters and error > tol:
				xnew = Inital + np.linalg.solve(Hessian(*Inital), -Gradient(*Inital)).T[0]
				error = np.abs((xnew - Inital).sum())
				Inital = xnew
				iteration += 1
			funcValue = Function(*Inital)
		else:
			while iteration < max_iters and error > tol:
				xnew = Inital + np.linalg.solve(Hessian(*Inital,*Args), -Gradient(*Inital,*Args)).T[0]
				error = np.abs((xnew - Inital).sum())
				Inital = xnew
				iteration += 1
			funcValue = Function(*Inital,*Args)

		if error < tol:
			return 'Error Tolerance Met', Inital, funcValue, iteration+1
		else:
			return 'Iteration Limit Met', Inital, funcValue, iteration+1

	def SimulatedAnealing(Prob, bounds, max_iters=100, Args=''):

		T0 		= 90
		Tf 		= 0.1
		alpha 	= 0.01

		Tc		= T0

		currentState 	= General_Routines.Create_Vector(bounds)
		currentFitness 	= General_Routines.Calculate_Fitness(currentState, Prob, Args=Args)
		while Tc >= Tf:
			for i in range(max_iters):
				newState 	= General_Routines.Create_Vector(bounds)
				newFitness	= General_Routines.Calculate_Fitness(newState, Prob, Args=Args)

				if General_Routines.Probability_SA(currentFitness, newFitness, Tc) >= np.random.rand():
					currentState	= newState
					currentFitness	= newFitness

			Tc -= alpha

		return currentState, currentFitness

	def Constrained_Simulated_Annealing(Prob, Bounds, Constraint_Dict, max_iters=100, Args=''):

		T0		= 90
		Tf		= 0.1
		alpha	= 0.01

		Tc		= T0

		fGenerate 	= True
		Regens		= 4
		count		= 0
		while fGenerate and count < Regens:
			count			+= 1
			currentState 	= General_Routines.Create_Vector(Bounds)
			Mutant_Pen		= np.asarray([General_Routines.Calculate_Penalty_ineq(currentState, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']])
			Mutant_Pen		= np.where(Mutant_Pen< 1e-12, 1e-12, Mutant_Pen)
			Mutant_Pen_Scalar	= Mutant_Pen.sum()
			if Mutant_Pen_Scalar <= 1e-4:
				fGenerate	= False

		currentFitness		= General_Routines.Calculate_Fitness(currentState, Prob, Args=Args)

		while Tc >= Tf:
			for i in range(max_iters):
				fGenerate 	= True
				count		= 0
				while fGenerate and count < Regens:
					count		+= 1
					newState	= General_Routines.Create_Vector(Bounds)
					Mutant_Pen		= np.asarray([General_Routines.Calculate_Penalty_ineq(newState, Con, Args=Args) for Con in Constraint_Dict['ConstraintEqs']])
					Mutant_Pen		= np.where(Mutant_Pen< 1e-12, 1e-12, Mutant_Pen)
					Mutant_Pen_Scalar	= Mutant_Pen.sum()
					if Mutant_Pen_Scalar <= 1e-4:
						fGenerate	= False
				newFitness		= General_Routines.Calculate_Fitness(newState, Prob, Args=Args)

				if General_Routines.Probability_SA(currentFitness, newFitness, Tc) >= np.random.rand():
					currentState	= newState
					currentFitness	= newFitness

			Tc -= alpha

		return currentState, currentFitness

class ODE:

	def Forward_Euler(Prob, t0, x0, dt, maxTime):

		ttime = t0
		timeList = [t0]
		funcList = [x0]
		while ttime <= maxTime:
			x0 = x0 + Prob(x0) * dt
			ttime += dt
			timeList.append(ttime); funcList.append(x0)

		return np.asarray(timeList), np.asarray(funcList)

	def Backward_Euler(Prob, t0, x0, dt, maxTime):

		ttime = t0
		timeList = [t0]
		funcList = [x0]
		while ttime <= maxTime:
			ttime += dt
			x0 = General_Optimization.Secant_Method(Prob, x0,x0+1,Args=[x0,dt])
			timeList.append(ttime)
			funcList.append(x0)

		return np.asarray(timeList), np.asarray(funcList)

	def RK4(f, y, t, h):

		k1 = f(y,t)
	    k2 = f(y + h * (k1/2), t + h/2)
	    k3 = f(y + h * (k2/2), t + h/2)
	    k4 = f(y + h*k3, t + h)

	    return y + (1/6) * h * (k1 + 2*k2 + 2*k3 + k4)

	def Adams_Bashforth(f, yList, tList, h, order='2nd'):

		if order=='2nd':
			return yList[-1] + h * ((3/2) * f(yList[-1], tList[-1]) - (1/2) * f(yList[-2], tList[-2]))
		elif order == '3rd':
			return yList[-1] + h * ((23/12) * f(yList[-1], tList[-1]) - (16/12) * f(yList[-2],tList[-2]) + (5/12) * f(yList[-3], tList[-3]))
		elif order == '4th':
			return yList[-1] + (h/24) * (55 * f(yList[-1], tList[-1] - 59 * f(yList[-2], tList[-2]) + 37 * f(yList[-3], tList[-3]) - 9 * f(yList[-4],tList[-4])))
		elif order == '5th':
			return yList[-1] + (h/720) * (1901 * f(yList[-1], tList[-1]) - 2774 * f(yList[-2], tList[-2]) + 2616 * f(yList[-3], tList[-3]) - 1274 * f(yList[-4],tList[-4]) + 251 * f(yList[-5],tList[-5]))

class NEB:

	def Lambdify_Function_Derivatives(VarList, FuncList, Func):

		gradV	= []
		for Var in VarList:
			gradV.append(sp.lambdify(FuncList, Func.diff(Var), 'numpy'))

		lambda_V	= sp.lambdify(FuncList, Func, 'numpy')

		return lambda_V, gradV

	def Create_String_Images(Loc1, Loc2, numImages):

		Images = [Loc1]

		for i in range(1, numImages+1):
			Images.append(Loc1 + (i / (numImages + 1)) * (Loc2 - Loc1))

		Images.append(Loc2)

		return Images

	def Calculate_Spring_Force(Images, N, Tangent, springConstant):

		rightImage 	= Images[N+1]
		leftImage	= Images[N-1]
		cImage		= Images[N]

		springTerm = springConstant * (np.linalg.norm(rightImage - cImage) -
									np.linalg.norm(cImage - leftImage)) * Tangent

		return springTerm

	def Find_Tangent_Vector(Images, N):

		rightImage	= Images[N+1]
		leftImage	= Images[N-1]
		cImage		= Images[N]

		tan			= (cImage - leftImage) / np.linalg.norm(cImage - leftImage)
		tan			+= (rightImage - cImage) / np.linalg.norm(rightImage - cImage)

		return tan / np.linalg.norm(tan)

	def True_Force(Image, Gradient, Args=''):

		gradV = []
		for i in range(len(Gradient)):
			if Args != '':
				gradV.append(Gradient[i](*np.concatenate((Image,Args))))
			else:
				gradV.append(Gradient[i](*Image))

		return gradV

	def Orthogonal_Force(gradForce, Tangent):

		return gradForce - np.dot(gradForce, Tangent) * Tangent

	def Update_Images(Images, numImages, Gradient, springConstant, Args=''):

		#All of the gradForces have to be normalized to the Energy Landscape
		#Therefore we are adopting a slightly different method for Updating Images
		gradForces		= []
		tangentVectors	= []
		springForces	= []
		for nImage in range(1,numImages+1):
			gradForces.append(NEB.True_Force(Images[nImage], Gradient, Args=Args))
			tangentVectors.append(NEB.Find_Tangent_Vector(Images, nImage))
			springForces.append(NEB.Calculate_Spring_Force(Images, nImage, tangentVectors[nImage-1], springConstant))

		gradForces = np.vstack(gradForces)

		gradMeans = (np.mean(np.abs(gradForces),axis=0))

		for col in range(gradForces.shape[1]):
			if gradMeans[col] > 1e-5:
				gradForces[:,col] = gradForces[:,col] / gradMeans[col]


		newImages = [Images[0]]
		for tanV, gF, sF, Image in zip(tangentVectors, gradForces, springForces, Images[1:numImages+1]):
			orthogonalForce = NEB.Orthogonal_Force(gF, tanV)
			totalForce		= sF - orthogonalForce
			newImages.append(Image + 0.01 * totalForce)

		newImages.append(Images[numImages+1])

		return np.vstack(newImages)

	def Plot_Images(Func, Images, Args=''):

		imageVec	= [i for i in range(len(Images))]
		if Args != '':
			fValues = [Func(*np.concatenate((ind,Args))) for ind in Images]
		else:
			fValues		= [Func(*ind) for ind in Images]

		plt.plot(imageVec, fValues,'-o')
		plt.show()

	def Find_Max_Image(Func, Images, Args=''):

		if Args != '':
			maxi 	= Func(*np.concatenate((Images[0],Args)))
		else:
			maxi 		= Func(*Images[0])
		maxImage 	= Images[0]
		for image in Images[1:]:
			if Args != '':
				fImage	= Func(*np.concatenate((image,Args)))
			else:
				fImage 	= Func(*image)
			if fImage > maxi:
				maxi 		= fImage
				maxImage	= image

		return maxi, maxImage

	def ReportValues(Func, Images, Args=''):

		returnReport = []

		for im in Images:
			if Args != '':
				returnReport.append(Func(*im,*Args))
			else:
				returnReport.append(Func(*im))

		return returnReport

	def NEB(Func, VarList, FuncList, Loc1, Loc2, springConstant=1.0, numImages=10, iters=20, Args='', savePath=False):

		#Function Must Be Provided along with the Variable List
		#Should be in the form of a Sympy Add Object (Symbolic Form)

		l_Func, Gradient 	= NEB.Lambdify_Function_Derivatives(VarList, FuncList, Func)

		Images				= NEB.Create_String_Images(Loc1, Loc2, numImages)
		Images				= np.vstack(Images)
		if savePath:
			funcVs = NEB.ReportValues(l_Func, Images, Args=Args)
			returnPath = []
			for im, fV in zip(Images, funcVs):
				returnPath.append([*im,fV])

		# X, Y = np.meshgrid(np.linspace(-0.75, 0.75, 20), np.linspace(-0.75, 0.75, 20))
		# Z = l_Func(*np.concatenate(([X,Y,0],Args)))
		# plt.contourf(np.linspace(-0.75, 0.75, 20),np.linspace(-0.75, 0.75, 20),Z,50,cmap='rainbow')
		# plt.colorbar()
		# plt.scatter(Loc1[0], Loc1[1],color='k')
		# plt.scatter(Loc2[0], Loc2[1],color='k')
		# plt.savefig('OG_Pic.png')
		# plt.close()
		#
		# X, Y = np.meshgrid(np.linspace(-0.60, 0.60, 20), np.linspace(-0.60, 0.60, 20))
		# Z = l_Func(*np.concatenate(([X,Y,0],Args)))
		# plt.contourf(np.linspace(-0.60, 0.60, 20),np.linspace(-0.60, 0.60, 20),Z,50,cmap='rainbow')
		# plt.colorbar()
		# plt.scatter(Images[:,0], Images[:,1],color='k')
		# plt.close()

		for i in range(iters):
			Images			= NEB.Update_Images(Images, numImages, Gradient, springConstant, Args=Args)
			if savePath:
				funcVs = NEB.ReportValues(l_Func, Images, Args=Args)
				for im, fV in zip(Images, funcVs):
					returnPath.append([*im,fV])

			# X, Y = np.meshgrid(np.linspace(-0.75, 0.75, 20), np.linspace(-0.75, 0.75, 20))
			# Z = l_Func(*np.concatenate(([X,Y,0],Args)))
			# plt.contourf(np.linspace(-0.75, 0.75, 20),np.linspace(-0.75, 0.75, 20),Z,50,cmap='rainbow')
			# plt.colorbar()
			# plt.scatter(Images[:,0], Images[:,1],color='k')
			# plt.savefig('Pic_{}.png'.format(i))
			# plt.close()

		#NEB.Plot_Images(l_Func, Images, Args=Args)

		fSaddle, locSaddle = NEB.Find_Max_Image(l_Func, Images, Args=Args)

		if savePath:
			return locSaddle, fSaddle, Images, returnPath
		else:
			return locSaddle, fSaddle, Images

class Numerical_Integration:

	def Monte_Carlo():

		print('Need to Implement')

	def Trapizodal_Integration():

		print('Need to Implement')

	def Simpson_Integration():

		print('Need to implement')
