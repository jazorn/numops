# Numerical Optimization (NumOps)

Numerical Optimization and Algorithms for Python
Written by Jacob Zorn
Version: 0.2.0

Overview:

A collection of numerical algorithms built with Numpy and Sympy in mind for
Nonlinear Engineering Optimization Applications. There currently exist four classes
in the suite: General Routines, Differential Evolution, General Optimization, and 
Nudged Elastic Band. Each of the classes are described below.

1. [Differential Evolution](#DE)
2. [General Optimization](#GO)
3. [Nudged Elastic Band](#NEB)
4. [Numerical Integration](#Numerical Integration)
5. [Ordinary Differential Equations](#ODE)
6. [Future Work](#Future)
7. [Licensing](#License)
8. [Acknowledgments](#Thanks)

## Differential Evolution
# <a name="DE"></a>

Differential evolution is a stochastic, gradient-free optimization method originally
proposd by Price and Storn [link here]. It has been proved to be highly efficient 
and effective method for solving nonlinear optimization method. It has since been
added on and evolved into a number of variants for solving a number of problem types.

Specifically, inside of NumOps, 4 Variants of the Canonical Differential Evolution algorithm has been implemented:

1. Differential Evolution
2. [Multimodal Differential Evolution (Nearest Neighbor Implementation)](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=6557556&casa_token=ZuMaXzERqYEAAAAA:e3fTFxv66wfjBNQ_lswdfJAkMROvIvKxEHfuzi_wdB8Lj4D-qChpa6LeKCSPOEay5WSUOw8y)
3. [Constrained Differential Evolution](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=1688283&casa_token=GXnOWSjlZasAAAAA:ScdekkyLmaxlkKPEJylU9L9VFOBk_uYfe-HWuUpFyOMVJLzYH1cLfWcFUpwpdnxxIQWuo5cc)
4. Regression Differential Evolution
5. JADE Differential Evolution
6. SQG-Differential Evolution (Stochastic Quasi-Gradient)

Within each type of Differential Evolution, a variety of settings can be manipulated such as,

+ Implementation of Gradient Assist Schemes
+ Implementation of Information Preservation Schemes
+ Number of Candidate Vectors
+ Mutation and Crossover Parameters

As NumOps grows, additional evolutionary optimizaton techniques will be added. Some planned variants include

1. [Self-Adaptive Differential Evolution](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=1554904&casa_token=528TMwk8mJ4AAAAA:o_NyJKQ9C5Uc9Z5HFuaEUH_VKDLf_BWzN5hPcSof7Ry3DEUVpsTeN0a78c78_SAndg8KzLyJ&tag=1)
2. [Simulated Annealing Differential Evolution](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=4028411&casa_token=U-XoIcLgQYUAAAAA:OaQrrDSPUD1BpYlDM1Sjx6SKDIa575CTPiXC5iG8vsyAPz6nXZRCTZvthz_FjGoMvGDQQnu8)
3. [Hill-Valley Differential Evolution](https://content.iospress.com/articles/international-journal-of-hybrid-intelligent-systems/his220)
4. [Cuckoo Search](https://link.springer.com/article/10.1007/s00521-013-1367-1)
5. [Firefly Search](https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=7193181&casa_token=fD1WOoWYDMQAAAAA:u8HC3dZr0DfjUJ3W7uUZI1TNlS9VX5fhJU4nLnZOQXqJBSmORoIm3tOjlqdv9exTIrSGoCu8)

## General Optimization
# <a name="GO"></a>

A variety of optimization techniques are utilized here such as Newton-Raphson and Bisection Method. These are designed for root-finding or to identify the location of function extrema. This is controllable via a variety of kwargs for each of the techniques.

1. [Bisection Method](https://oregonstate.edu/instruct/mth251/cq/Stage4/Lesson/bisection.html)
2. [Gradient Descrent](https://arxiv.org/pdf/1609.04747.pdf)
3. [Secant Method](https://en.wikipedia.org/wiki/Secant_method)
4. [Newton's Method for Optimization](https://en.wikipedia.org/wiki/Newton%27s_method_in_optimization)
5. [Newton-Raphson's Method](https://en.wikipedia.org/wiki/Newton%27s_method)

Additional methods will be added in time. Currently, it has been decided that the following algorithms will be implemented for root-finding and extrema finding.

1. [BFGS Search](https://en.wikipedia.org/wiki/Broyden%E2%80%93Fletcher%E2%80%93Goldfarb%E2%80%93Shanno_algorithm)
2. [Simulated Annealing](https://link.springer.com/chapter/10.1007/978-94-015-7744-1_2)
3. [Broyden Method](https://en.wikipedia.org/wiki/Broyden%27s_method)
4. [Conjugate Gradient](https://en.wikipedia.org/wiki/Conjugate_gradient_method)

## Nudged Elastic Band
# <a name="NEB"></a>

The Nudged Elastic Band method is useful for finding saddle points and minimum
energy pathways in an energy landscape. This can prove useful for a variety of
different problems, varying from identifying metastable points in an energy landscape.

Upcoming Additions: Distortion Symmetry Elastic Bands

## Numerical Integration
# <a name="Numerical Integration"></a>

1. [Monte Carlo Integration](https://en.wikipedia.org/wiki/Monte_Carlo_integration)
2. [Trapizodal Integration](https://en.wikipedia.org/wiki/Trapezoidal_rule)
3. [Simpsons Integration](https://en.wikipedia.org/wiki/Simpson%27s_rule)

## Integration of Differential Equations
# <a name="ODE"></a>

Differential equations are an intregral part of scientific discovery and as such, it is important to develop highly optimized and useful solvers for calculating them. We have implemented a number of methods for these equations for initial boundary problems and boundary value problems.

1. [Forward Euler](https://en.wikipedia.org/wiki/Euler_method) 
2. [Backward Euler](https://en.wikipedia.org/wiki/Backward_Euler_method)
3. [Runge-Kutta Methods](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods)
4. [Adams-Bashforth Methods](https://en.wikipedia.org/wiki/Linear_multistep_method#Adams%E2%80%93Bashforth_methods)

In the future, it is planned to implement additional methods for solving differential evolutions efficiently such as,

1. [Predictor-Corrector Methods](https://en.wikipedia.org/wiki/Predictor%E2%80%93corrector_method)


## General Routines
# <a name="General"></a>

This class just holds a variety of routines called often by the various solving
methods in this library.

## Future Work
# <a name="Future"></a>

In the future the following routines and additions will be made to the library.

1. Integer Programming 
2. Machine Learning Algorithms
3. Distributed Newtonian Methods
4. Parallelization and Accelerator Implementation

## Licensing
# <a name="License"></a>

The library is licensed by the GNL GPL V3 and included in the accompanying
license.txt file. Any use of this software must abide by the license.

## Acknowledgment
# <a name="Thanks"></a>

Pieces of this software were written as part of the XXX Contract with the United
States Department of Energy XXX. 

## Conflict of Interest

There are no known conflicts of interest known by the author in conjuction with
this software.
